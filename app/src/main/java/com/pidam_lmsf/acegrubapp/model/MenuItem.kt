package com.pidam_lmsf.acegrubapp.model

data class MenuItem(val nombre: String, val precio: Float, val descripcion: String)
