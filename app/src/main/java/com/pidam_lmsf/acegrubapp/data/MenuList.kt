package com.pidam_lmsf.acegrubapp.data
import com.pidam_lmsf.acegrubapp.model.MenuItem

class MenuList {
    fun getMenuList(): List<MenuItem> {
        return listOf<MenuItem>(
            MenuItem("Hamburguesa Sencilla", 15.99f, "Placeholder text"),
            MenuItem("Hamburguesa Deluxe", 18.99f, "Placeholder text"),
            MenuItem("Pizza de Pepperoni", 15.99f, "Placeholder text"),
            MenuItem("Pizza de Tres Carnes", 19.99f, "Placeholder text"),
            MenuItem("Papas Fritas", 9.99f, "Placeholder text"),
            MenuItem("Pie de Limon", 11.99f, "Placeholder text"),
        )
    }
}