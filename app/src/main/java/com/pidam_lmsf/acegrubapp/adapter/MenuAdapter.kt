package com.pidam_lmsf.acegrubapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.pidam_lmsf.acegrubapp.R
import com.pidam_lmsf.acegrubapp.model.MenuItem

class MenuAdapter(
    private val context: Context,
    private val menuList: List<MenuItem>,
    private val numItems: Int = menuList.size,
    private val listener: (MenuItem) -> Unit
    ) : RecyclerView.Adapter<MenuAdapter.MenuViewHolder>() {


    class MenuViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardView: CardView = view.findViewById(R.id.itemMenu)
        val textTitulo: TextView = view.findViewById(R.id.textTitulo2)
        val textPrecio: TextView = view.findViewById(R.id.textPrecio2)

        fun bind(item: MenuItem) {
            textTitulo.text = item.nombre
            textPrecio.text = "$ ${item.precio}"
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.card_layout2, parent, false)

        return MenuViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val itemMenu = menuList[position]

        holder.bind(itemMenu)

        holder.itemView.setOnClickListener { listener(itemMenu) }


    }

    override fun getItemCount() = numItems

}